Some Steps to run the project -


From this project root directory

`docker-compose build --no-cache`  
`docker-compose up -d`

## Bringing down containers

`docker-compose down`   

## Accessing tomcat

In web browser go to http://<ipaddress>:5000.